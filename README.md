# breadly

A timer app for long baking projects

## Getting Started

This project was created using
* [Flutter](https://flutter.dev/docs)
* [Moor](https://moor.simonbinder.eu/docs/)

## Code generation

Translations are generated automatically (see `pubspec.yaml` => `flutter.generate: true`).

Moor database classes are generate manually by running `flutter pub run build_runner build`.
Don't forget to create migrations!