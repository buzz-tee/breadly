import 'package:breadly/pages/about.dart';
import 'package:breadly/pages/routes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AppDrawer extends StatelessWidget {
  final ValueChanged<Section> onNavSection;

  AppDrawer({required this.onNavSection});

  Widget _createHeader(BuildContext context) {
    return DrawerHeader(
      decoration: BoxDecoration(
        color: Colors.blue,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Text(
            AppLocalizations.of(context)!.app_title,
            style: Theme.of(context)
                .textTheme
                .headline3!
                .apply(color: Colors.white),
          ),
          Text(
            AppLocalizations.of(context)!.app_subtitle,
            style: Theme.of(context)
                .textTheme
                .subtitle1!
                .apply(color: Colors.white),
          ),
        ],
      ),
    );
  }

  Widget _createItem(String text, Section section) {
    return ListTile(
        title: Text(text),
        onTap: () {
          onNavSection(section);
        });
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          _createHeader(context),
          _createItem(AppLocalizations.of(context)!.nav__jobs, Section.JOBS),
          _createItem(
              AppLocalizations.of(context)!.nav__profiles, Section.PROFILES),
          Divider(),
          ListTile(
            title: Text(AppLocalizations.of(context)!.nav__about),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                return AboutScreen();
              }));
            },
          ),
        ],
      ),
    );
  }
}
