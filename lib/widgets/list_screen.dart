import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

abstract class CustomListScreen extends StatefulWidget {
  final List<VoidCallback> _onAction = [];

  void addActionCallback(VoidCallback callback) => _onAction.add(callback);
  void removeActionCallback(VoidCallback callback) =>
      _onAction.remove(callback);

  @protected
  void notifyActionCallbacks() => _onAction.forEach((c) {
        c.call();
      });

  Widget? get floatingActionButton => FloatingActionButton(
        onPressed: () => notifyActionCallbacks(),
        child: Icon(Icons.add),
      );

  String getTitle(final BuildContext context);
}

abstract class CustomListScreenState<S extends CustomListScreen, T>
    extends State<S> {
  @protected
  String get emptyListText;

  Future<List<T>> loadItems();

  String getItemTitle(final T item);
  List<InlineSpan>? getItemSubTitle(final T item);

  Widget Function(BuildContext) getPageBuilder(T? item);

  final items = <T>[];

  // convenience access to AppLocalizations item
  @protected
  AppLocalizations get locale => AppLocalizations.of(context)!;

  @override
  void initState() {
    super.initState();
    widget.addActionCallback(onActionButton);
    refreshItems();
  }

  @override
  void didUpdateWidget(covariant S oldWidget) {
    super.didUpdateWidget(oldWidget);
    oldWidget.removeActionCallback(onActionButton);
    widget.addActionCallback(onActionButton);
  }

  @override
  void dispose() {
    super.dispose();
    widget.removeActionCallback(onActionButton);
  }

  void onActionButton() async {
    await Navigator.push(
      context,
      MaterialPageRoute(builder: getPageBuilder(null)),
    );
    refreshItems();
  }

  @protected
  void refreshItems() {
    loadItems().then((loadedItems) {
      setState(() {
        items.clear();
        items.addAll(loadedItems);
      });
    });
  }

  Widget? _buildItemSubTitle(T item) {
    final spans = getItemSubTitle(item);
    if (spans?.isEmpty ?? true)
      return null;
    else {
      final textTheme = Theme.of(context).textTheme;
      return RichText(
          text: TextSpan(
              children: spans,
              style:
                  textTheme.subtitle2?.apply(color: textTheme.caption?.color)));
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<T>>(
      future: loadItems(),
      builder: (context, snapshot) {
        if (snapshot.hasError)
          return Center(child: Text('Error loading items...'));

        items.clear();
        if (snapshot.hasData) items.addAll(snapshot.data!);

        if (snapshot.connectionState == ConnectionState.waiting)
          return Center(child: CircularProgressIndicator());

        if (items.length == 0) return Center(child: Text(emptyListText));

        return ListView.separated(
          itemBuilder: (context, index) {
            if (index >= items.length) return SizedBox.shrink();
            final item = items[index];
            return ListTile(
                title: Text(getItemTitle(item)),
                subtitle: _buildItemSubTitle(item),
                onTap: () {
                  Navigator.of(context)
                      .push(
                        MaterialPageRoute(builder: getPageBuilder(item)),
                      )
                      .then((_) => refreshItems());
                });
          },
          separatorBuilder: (context, index) => Divider(),
          itemCount: items.length,
        );
      },
    );
  }
}
