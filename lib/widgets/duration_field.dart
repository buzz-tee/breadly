import 'package:breadly/model/model.dart';
import 'package:breadly/widgets/duration_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class DurationField extends StatefulWidget {
  final bool editMode;
  final FlexibleDuration duration;
  final ValueChanged<FlexibleDuration>? onChanged;

  DurationField({
    required this.editMode,
    required this.duration,
    this.onChanged,
  }) : super();

  @override
  State<StatefulWidget> createState() => _DurationFieldState();
}

class _DurationFieldState extends State<DurationField> {
  late FlexibleDuration duration;

  @override
  void initState() {
    super.initState();
    duration = widget.duration;
  }

  @override
  void didUpdateWidget(DurationField oldWidget) {
    super.didUpdateWidget(oldWidget);
    duration = widget.duration;
  }

  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: TextButton.styleFrom(
        visualDensity: VisualDensity(vertical: VisualDensity.minimumDensity),
      ),
      child: Text(duration.toLocalizedString(AppLocalizations.of(context)!)),
      onPressed: widget.editMode
          ? () {
              showDialog(
                context: context,
                builder: (context) => DurationDialog(duration),
              ).then((value) {
                setState(() {
                  duration = value;
                });
                widget.onChanged?.call(value);
              });
            }
          : null,
    );
  }
}
