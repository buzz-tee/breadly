import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sprintf/sprintf.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

abstract class CustomDetailsScreen<T> extends StatefulWidget {
  final T? item;
  final bool editMode;

  CustomDetailsScreen({
    this.item,
    this.editMode = false,
  });
}

class AlertDialogButton {
  final String caption;
  final Color? color;
  final dynamic result;

  AlertDialogButton({required this.caption, this.color, required this.result});

  static List<AlertDialogButton> getDangerConfirmButtons(
          BuildContext context) =>
      [
        AlertDialogButton(
            caption: AppLocalizations.of(context)!.generic__confirm,
            result: true,
            color: Colors.red),
        AlertDialogButton(
            caption: AppLocalizations.of(context)!.generic__cancel,
            result: false),
      ];
  static List<AlertDialogButton> getOkButton(BuildContext context) => [
        AlertDialogButton(
            caption: AppLocalizations.of(context)!.generic__ok, result: true),
      ];
  static List<AlertDialogButton> getOkCancelButtons(BuildContext context) => [
        AlertDialogButton(
            caption: AppLocalizations.of(context)!.generic__ok, result: true),
        AlertDialogButton(
            caption: AppLocalizations.of(context)!.generic__cancel,
            result: false),
      ];
}

abstract class CustomDetailsScreenState<S extends CustomDetailsScreen<T>, T>
    extends State<S> {
  // is widget in edit mode?
  @protected
  late bool editMode;
  // current item, null will display new item form
  @protected
  late T? item;

  // key to validate the form
  final _formKey = GlobalKey<FormState>();

  final _scrollController = ScrollController();

  @protected
  String get itemName;
  // title for the new item form
  @protected
  String get newItemTitle;
  // title for the edit item form, may include the item name
  @protected
  String get editItemTitle;
  // title for the view item form, may include the item name
  @protected
  String get viewItemTitle;

  // update the form state from current item
  @protected
  void refreshModelFromItem();
  // generate a new item from the form state
  @protected
  T generateItemFromModel();

  // build the main form
  @protected
  List<Widget> buildForm();

  // check if item can be deleted
  @protected
  Future<bool> checkDeleteAllowed();

  // check if item can be edited
  @protected
  Future<bool> checkEditAllowed();

  // actually delete the item
  @protected
  Future<bool> deleteItem();
  // actually save the item
  @protected
  Future<bool> saveItem();

  @protected
  FormState? get formState => _formKey.currentState;

  // convenience access to AppLocalizations item
  @protected
  AppLocalizations get locale => AppLocalizations.of(context)!;

  @override
  void initState() {
    super.initState();

    editMode = widget.editMode;
    item = widget.item;

    refreshModelFromItem();
  }

  @protected
  void scrollToEnd() {
    _scrollController.animateTo(_scrollController.position.maxScrollExtent,
        duration: Duration(milliseconds: 500), curve: Curves.fastOutSlowIn);
  }

  void simpleSnackBar(final String text) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(text)));
  }

  AlertDialog Function(BuildContext) simpleAlertDialogBuilder(
      final String title,
      final Widget content,
      final List<AlertDialogButton> buttons) {
    return (context) =>
        AlertDialog(title: Text(title), content: content, actions: [
          for (final button in buttons)
            TextButton(
              child: Text(
                button.caption,
                style: button.color != null
                    ? TextStyle(color: button.color)
                    : null,
              ),
              onPressed: () => Navigator.of(context).pop(button.result),
            )
        ]);
  }

  AlertDialog Function(BuildContext) textAlertDialogBuilder(final String title,
      final String content, final List<AlertDialogButton> buttons) {
    return simpleAlertDialogBuilder(title, Text(content), buttons);
  }

  String? Function(String?)? valueRequired(final String errorMessage) =>
      (value) => (value == null || value.isEmpty) ? errorMessage : null;

  Widget buildTextField(
      final String labelText, final TextEditingController controller,
      {final String? Function(String?)? validator,
      final bool autofocus = false,
      final FocusNode? focusNode}) {
    return (!editMode && controller.text.isEmpty)
        ? SizedBox.shrink()
        : TextFormField(
            focusNode: focusNode,
            readOnly: !editMode,
            autofocus: autofocus,
            decoration: InputDecoration(
              labelText: labelText,
            ),
            validator: validator,
            controller: controller,
          );
  }

  void _onSave() {
    if (formState!.validate()) {
      final isNew = item == null;
      item = generateItemFromModel();

      saveItem().then((saveResult) {
        if (saveResult) {
          if (isNew) {
            editMode = false;
            Navigator.of(context).pop(item);
          } else {
            setState(() {
              editMode = false;
            });
          }
        } else {
          simpleSnackBar(locale.generic__save_failed);
        }
      });
    }
  }

  void _onEdit() {
    checkEditAllowed().then((allowed) {
      if (allowed) {
        setState(() {
          refreshModelFromItem();
          editMode = true;
        });
      }
    });
  }

  void _onDelete() {
    checkDeleteAllowed().then((allowed) {
      if (allowed) {
        deleteItem().then((res) {
          if (res) {
            editMode = false;
            Navigator.of(context).pop();
          } else {
            simpleSnackBar(
                AppLocalizations.of(context)!.generic__delete_failed);
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> actions = [];
    String title;
    if (editMode) {
      actions.add(
        Padding(
          padding: EdgeInsets.only(right: 20.0),
          child: GestureDetector(
            onTap: _onSave,
            child: Icon(Icons.check),
          ),
        ),
      );

      if (item == null) {
        title = newItemTitle;
      } else {
        title = sprintf(editItemTitle, [itemName]);
      }
    } else {
      actions.add(
        Padding(
          padding: EdgeInsets.only(right: 20.0),
          child: GestureDetector(
            onTap: _onEdit,
            child: Icon(Icons.edit),
          ),
        ),
      );

      title = sprintf(viewItemTitle, [itemName]);
    }
    if (item != null) {
      actions.add(
        Padding(
          padding: EdgeInsets.only(right: 20.0),
          child: GestureDetector(
            onTap: _onDelete,
            child: Icon(Icons.delete),
          ),
        ),
      );
    }

    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          title: Text(title),
          actions: actions,
        ),
        body: SafeArea(
          minimum: EdgeInsets.all(10.0),
          child: Form(
            key: _formKey,
            child: ListView(
              controller: _scrollController,
              children: buildForm(),
            ),
          ),
        ),
      ),
      onWillPop: () {
        if (editMode && item != null) {
          setState(() {
            refreshModelFromItem();
            editMode = false;
          });
          return Future.value(false);
        }
        return Future.value(true);
      },
    );
  }
}
