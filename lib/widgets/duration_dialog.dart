import 'package:breadly/model/model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'extended_cupertino_timer_picker.dart';

class DurationDialog extends StatefulWidget {
  final FlexibleDuration? duration;

  DurationDialog(this.duration);

  State<StatefulWidget> createState() => _DurationDialogState();
}

class _DurationDialogState extends State<DurationDialog> {
  late int duration;
  late int? maxDuration;
  late bool flexibleDuration;
  bool maxDurationConstraintFailed = false;

  @override
  initState() {
    super.initState();
    duration = widget.duration?.duration ?? 0;
    maxDuration = widget.duration?.maxDuration;
    flexibleDuration = (maxDuration != null);
  }

  AppLocalizations get locale => AppLocalizations.of(context)!;

  Widget _buildCupertinoPicker(
          {required final void Function(Duration value) onChanged}) =>
      Container(
        height: MediaQuery.of(context).size.height * 0.20,
        width: MediaQuery.of(context).size.width * 0.75,
        child: CupertinoTheme(
          data: CupertinoThemeData(
            textTheme: CupertinoTextThemeData(
              pickerTextStyle: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          child: ExtendedCupertinoTimerPicker(
            maxTimerDuration: Duration(days: 10),
            initialTimerDuration: Duration(minutes: duration),
            mode: CupertinoTimerPickerMode.hm,
            onTimerDurationChanged: onChanged,
          ),
        ),
      );

  List<Widget> _buildRows() {
    final titleStyle =
        Theme.of(context).textTheme.subtitle2 ?? TextStyle(fontSize: 15);

    List<Widget> rows = [
      Text(
        locale.duration__standard_duration,
        style: titleStyle,
      ),
      _buildCupertinoPicker(
        onChanged: (value) {
          setState(() {
            duration = value.inMinutes;
          });
        },
      ),
      Row(
        children: [
          Checkbox(
            value: flexibleDuration,
            onChanged: (value) {
              setState(() {
                maxDurationConstraintFailed = false;
                flexibleDuration = !flexibleDuration;
                if (maxDuration == null || maxDuration! < duration)
                  maxDuration = duration;
              });
            },
          ),
          Text(
            locale.duration__flexible_duration_ask,
            style: titleStyle,
          ),
        ],
      ),
    ];
    if (flexibleDuration) {
      rows.addAll([
        Text(locale.duration__maximum_duration, style: titleStyle),
        _buildCupertinoPicker(
          onChanged: (value) {
            setState(() {
              maxDuration = value.inMinutes;
            });
          },
        ),
      ]);

      if (maxDurationConstraintFailed) {
        rows.add(Text(locale.duration__maximum_duration_constraint_failed,
            style: titleStyle.apply(color: Colors.red)));
      }
    }
    return rows;
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(locale.duration__duration),
      scrollable: true,
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _buildRows(),
      ),
      actions: [
        TextButton(
          child: Text(locale.generic__ok),
          onPressed: () {
            if (flexibleDuration && maxDuration! <= duration) {
              setState(() {
                maxDurationConstraintFailed = true;
              });
            } else {
              flexibleDuration &= (duration < (maxDuration ?? duration));
              Navigator.of(context).pop(FlexibleDuration(
                duration: duration,
                maxDuration: flexibleDuration ? maxDuration : null,
              ));
            }
          },
        ),
        TextButton(
          child: Text(locale.generic__cancel),
          onPressed: () => Navigator.of(context).pop(widget.duration),
        ),
      ],
    );
  }
}
