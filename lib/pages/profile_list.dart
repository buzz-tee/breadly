import 'package:breadly/model/model.dart';
import 'package:breadly/pages/profile_details.dart';
import 'package:breadly/widgets/list_screen.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';
import 'package:sprintf/sprintf.dart';

class ProfileListScreen extends CustomListScreen {
  @override
  getTitle(context) => AppLocalizations.of(context)!.profiles__title;

  @override
  createState() => _ProfileListScreenState();
}

class _ProfileListScreenState
    extends CustomListScreenState<ProfileListScreen, BreadlyProfile> {
  @override
  get emptyListText => locale.profiles__empty;

  @override
  loadItems() => context.read<BreadlyDatabase>().profilesDao.getAll();

  @override
  getItemTitle(profile) => profile.name ?? locale.profiles__unnamed_profile;

  @override
  getItemSubTitle(profile) {
    final spans = <InlineSpan>[];
    if (profile.description != null && profile.description!.isNotEmpty)
      spans.addAll([
        TextSpan(text: profile.description!),
        TextSpan(text: '\n'),
      ]);
    spans.add(TextSpan(
        text: sprintf(locale.profiles__total_duration,
            [profile.totalDuration.toLocalizedString(locale)])));
    return spans;
  }

  @override
  getPageBuilder(profile) => (context) =>
      ProfileDetailsScreen(profile: profile, editMode: profile == null);
}
