import 'package:breadly/model/model.dart';
import 'package:breadly/widgets/details_screen.dart';
import 'package:breadly/widgets/duration_field.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sprintf/sprintf.dart';
import 'package:breadly/util/alarmconfig.dart';

class UnfocussingReorderableDragStartListener
    extends ReorderableDragStartListener {
  const UnfocussingReorderableDragStartListener({
    Key? key,
    required Widget child,
    required int index,
  }) : super(key: key, child: child, index: index);

  @override
  Widget build(BuildContext context) {
    return Listener(
      onPointerDown: (PointerDownEvent event) {
        FocusScope.of(context).unfocus(disposition: UnfocusDisposition.scope);
        _startDragging(context, event);
      },
      child: child,
    );
  }

  void _startDragging(BuildContext context, PointerDownEvent event) {
    final SliverReorderableListState? list =
        SliverReorderableList.maybeOf(context);
    list?.startItemDragReorder(
      index: index,
      event: event,
      recognizer: createRecognizer(),
    );
  }
}

class ProfileDetailsScreen extends CustomDetailsScreen<BreadlyProfile> {
  ProfileDetailsScreen({
    BreadlyProfile? profile,
    bool editMode = false,
  }) : super(item: profile, editMode: editMode);

  @override
  createState() => _ProfileDetailsScreenState();
}

class _ProfileStepsController {
  final name = TextEditingController();
  final duration = FlexibleDuration();
  var alarmConfig = AlarmConfig.AlarmOnAutoAdvance;
}

class _ProfileDetailsScreenState
    extends CustomDetailsScreenState<ProfileDetailsScreen, BreadlyProfile> {
  final _name = TextEditingController();
  final _description = TextEditingController();
  final _steps = <_ProfileStepsController>[];
  final _controllersForDisposal = <TextEditingController>[];
  int _requestFocusIndex = -1;

  void clearStepsControllers() {
    _controllersForDisposal.forEach((element) => element.dispose());
    _controllersForDisposal.clear();
    _steps.forEach((step) => _controllersForDisposal.add(step.name));
    _steps.clear();
  }

  @override
  void dispose() {
    super.dispose();
    _name.dispose();
    _description.dispose();
    clearStepsControllers();
    _controllersForDisposal.forEach((element) => element.dispose());
    _controllersForDisposal.clear();
  }

  Widget _buildStepTile(BuildContext context, int index) {
    _ProfileStepsController step =
        index < _steps.length ? _steps[index] : _ProfileStepsController();

    final nameField = buildTextField(
      sprintf(locale.profiles__step_name_label, [index + 1]),
      step.name,
      validator: valueRequired(locale.profiles__step_name_missing),
      autofocus: editMode && (_requestFocusIndex == index),
    );

    final subtitleTextStyle = TextStyle(
      fontSize: Theme.of(context).textTheme.subtitle2?.fontSize ?? 15.0,
    );

    final alarmConfigWidget;
    if (editMode) {
      alarmConfigWidget = PopupMenuButton<AlarmConfig>(
        onSelected: (value) => setState(() {
          _steps[index].alarmConfig = value;
        }),
        padding: EdgeInsets.zero,
        icon: _steps[index].alarmConfig.getIcon(),
        itemBuilder: (context) => AlarmConfig.values
            .map<PopupMenuItem<AlarmConfig>>(
              (final alarmConfig) => PopupMenuItem(
                value: alarmConfig,
                child: RichText(
                  softWrap: false,
                  text: TextSpan(
                    style: Theme.of(context).textTheme.bodyText1,
                    children: [
                      WidgetSpan(child: alarmConfig.getIcon()),
                      TextSpan(text: alarmConfig.getText(locale)),
                    ],
                  ),
                ),
              ),
            )
            .toList(),
      );
    } else {
      alarmConfigWidget = Padding(
        padding: EdgeInsets.only(right: 5.0),
        child: _steps[index].alarmConfig.getIcon(),
      );
    }

    final durationRow = Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            alarmConfigWidget,
            Text(
              locale.profiles__step_after_label,
              style: subtitleTextStyle,
            ),
            DurationField(
                duration: step.duration,
                editMode: editMode,
                onChanged: (duration) {
                  step.duration.duration = duration.duration;
                  step.duration.maxDuration = duration.maxDuration;
                }),
          ],
        ),
      ],
    );

    if (editMode) {
      return ListTile(
        key: Key("ProfileStep-$index"),
        title: Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(right: 15.0),
              child: UnfocussingReorderableDragStartListener(
                index: index,
                child: Icon(Icons.drag_handle),
              ),
            ),
            Expanded(
              child: Column(
                children: <Widget>[
                  nameField,
                  durationRow,
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 15.0),
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    _steps.removeAt(index);
                  });
                },
                child: Icon(Icons.delete),
              ),
            ),
          ],
        ),
      );
    } else {
      return ListTile(
        key: Key("ProfileStepView-$index"),
        title: nameField,
        subtitle: Padding(
          child: durationRow,
          padding: EdgeInsets.only(top: 5.0),
        ),
      );
    }
  }

  @override
  buildForm() {
    final stepsHeader = Row(
      children: <Widget>[
        Expanded(
          child: Text(locale.profiles__steps_label,
              style: Theme.of(context).textTheme.caption),
        ),
      ],
    );
    if (editMode) {
      stepsHeader.children.add(
        GestureDetector(
          onTap: () {
            setState(() {
              _steps.add(_ProfileStepsController());
              _requestFocusIndex = _steps.length - 1;
            });
            WidgetsBinding.instance?.addPostFrameCallback((_) => scrollToEnd());
          },
          child: Ink(
            child: Icon(
              Icons.add,
              color: Theme.of(context).colorScheme.onPrimary,
            ),
            decoration: ShapeDecoration(
              shape: CircleBorder(),
              color: Theme.of(context).colorScheme.primary,
            ),
          ),
        ),
      );
    }

    return [
      buildTextField(
        locale.profiles__name_label,
        _name,
        autofocus: editMode && (_requestFocusIndex == -1),
        validator: valueRequired(locale.profiles__name_missing),
      ),
      buildTextField(locale.profiles__description_label, _description),
      Padding(
        child: stepsHeader,
        padding: EdgeInsets.only(top: 15.0),
      ),
      ReorderableListView.builder(
        shrinkWrap: true,
        physics:
            NeverScrollableScrollPhysics(), // descendant of SingleChildScrollView
        itemBuilder: _buildStepTile,
        itemCount: _steps.length,
        onReorder: (oldIndex, newIndex) {
          setState(() {
            if (oldIndex < newIndex) {
              newIndex -= 1;
            }
            final item = _steps.removeAt(oldIndex);
            _steps.insert(newIndex, item);
          });
        },
        buildDefaultDragHandles: false,
      ),
    ];
  }

  @override
  checkDeleteAllowed() {
    if (item == null) return Future.value(true);
    return context
        .read<BreadlyDatabase>()
        .profilesDao
        .usedBy(item!)
        .then((jobs) {
      if (jobs.isEmpty) {
        return showDialog(
          context: context,
          builder: textAlertDialogBuilder(
            locale.profiles__delete_caption,
            locale.profiles__delete_message,
            AlertDialogButton.getDangerConfirmButtons(context),
          ),
        ).then((result) => (result == true));
      } else {
        final result = StringBuffer(locale.profiles__used_by_jobs);
        for (final job in jobs) result.write('\n• ${job.name}');
        return showDialog(
          context: context,
          builder: textAlertDialogBuilder(
            locale.profiles__delete_caption,
            result.toString(),
            AlertDialogButton.getOkButton(context),
          ),
        ).then((_) => false);
      }
    });
  }

  @override
  Future<bool> checkEditAllowed() => Future.value(true);

  @override
  deleteItem() => item == null
      ? Future.value(true)
      : context.read<BreadlyDatabase>().profilesDao.remove(item!);

  @override
  get itemName => item?.name ?? locale.profiles__unnamed_profile;

  @override
  get editItemTitle => locale.profiles__edit;

  @override
  refreshModelFromItem() {
    _name.text = item?.name ?? '';
    _description.text = item?.description ?? '';

    clearStepsControllers();
    item?.steps.forEach((step) {
      _ProfileStepsController controller = _ProfileStepsController();
      controller.name.text = step.name.value;
      controller.duration.duration = step.duration.value;
      controller.duration.maxDuration = step.maxDuration.value;
      controller.alarmConfig = step.alarmConfig.value;
      _steps.add(controller);
    });
  }

  @override
  generateItemFromModel() {
    final newProfile = item ?? BreadlyProfile();
    newProfile.name = _name.text;
    newProfile.description = _description.text;

    newProfile.steps.clear();
    for (_ProfileStepsController step in _steps) {
      newProfile.addStep(
        name: step.name.text,
        duration: step.duration,
        alarmConfig: step.alarmConfig,
      );
    }
    return newProfile;
  }

  @override
  get newItemTitle => locale.profiles__new;

  @override
  saveItem() => item == null
      ? Future.value(false)
      : context.read<BreadlyDatabase>().profilesDao.save(item!);

  @override
  get viewItemTitle => locale.profiles__view;
}
