import 'package:breadly/model/model.dart';
import 'package:breadly/pages/job_details.dart';
import 'package:breadly/widgets/list_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:breadly/util/datetime.dart';
import 'package:breadly/util/alarmconfig.dart';

import 'package:sprintf/sprintf.dart';

class JobListScreen extends CustomListScreen {
  @override
  getTitle(context) => AppLocalizations.of(context)!.jobs__title;

  @override
  createState() => _JobListScreenState();
}

class _JobListScreenState
    extends CustomListScreenState<JobListScreen, BreadlyJob> {
  @override
  get emptyListText => locale.jobs__empty;

  @override
  loadItems() => context.read<BreadlyDatabase>().jobsDao.getAll();

  @override
  getItemTitle(job) => job.name ?? locale.jobs__unnamed_job;

  @override
  getItemSubTitle(job) {
    final spans = <InlineSpan>[
      TextSpan(
          style: TextStyle(fontWeight: FontWeight.bold),
          text: sprintf(locale.profiles__view, [
            job.profile.name.present
                ? job.profile.name.value
                : locale.profiles__unnamed_profile
          ]))
    ];

    final currentStep = job.getCurrentStep();
    final nextStep = job.getNextStep();
    if (currentStep != null || nextStep != null) {
      spans.add(TextSpan(text: '\n'));
      final DateTime nextStepStart;
      if (currentStep != null) {
        spans.addAll(
          [
            TextSpan(
                text: sprintf(locale.jobs__step_until,
                    [currentStep.name ?? locale.jobs__unnamed_step])),
            WidgetSpan(
              alignment: PlaceholderAlignment.middle,
              style: Theme.of(context).textTheme.caption,
              child: currentStep.alarmConfig.getIcon(
                  defaultColor: Theme.of(context).textTheme.caption!.color),
            ),
          ],
        );

        nextStepStart = currentStep.getLatestEndTime(job.currentStepStart);
      } else {
        spans.addAll([
          TextSpan(text: locale.jobs__step_start),
          WidgetSpan(
            alignment: PlaceholderAlignment.middle,
            child: Icon(Icons.alarm,
                color: Theme.of(context).textTheme.caption!.color),
          ),
        ]);
        nextStepStart = job.currentStepStart;
      }
      final formatter = nextStepStart.isOnSameDay(DateTime.now())
          ? DateFormat.jm(locale.localeName)
          : DateFormat.yMMMd(locale.localeName).add_jm();
      spans.add(TextSpan(
          text: sprintf(locale.jobs__step_then, [
        formatter.format(nextStepStart),
        nextStep != null
            ? (nextStep.name ?? locale.jobs__unnamed_step)
            : locale.jobs__step_finished
      ])));
    }

    return spans;
  }

  @override
  getPageBuilder(item) =>
      (context) => JobDetailsScreen(job: item, editMode: item == null);
}
