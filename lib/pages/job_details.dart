import 'package:breadly/model/model.dart';
import 'package:breadly/pages/profile_details.dart';
import 'package:breadly/widgets/details_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:breadly/util/datetime.dart';
import 'package:collection/collection.dart';
import 'package:sprintf/sprintf.dart';

class JobDetailsScreen extends CustomDetailsScreen<BreadlyJob> {
  JobDetailsScreen({
    BreadlyJob? job,
    bool editMode = false,
  }) : super(item: job, editMode: editMode);

  @override
  createState() => _JobDetailsScreenState();
}

class _JobDetailsScreenState
    extends CustomDetailsScreenState<JobDetailsScreen, BreadlyJob> {
  final _name = TextEditingController();
  final _profileReadOnly = TextEditingController();
  final _createNewProfile = BreadlyProfile();

  String _autoName = '';
  DateTime _schedule = DateTime.now();

  final _allProfiles = <BreadlyProfile>[];

  bool _profilesLoaded = false;
  BreadlyProfile? _selectedProfile;

  @override
  get itemName => item?.name ?? locale.jobs__unnamed_job;

  @override
  get newItemTitle => locale.jobs__new;

  @override
  get editItemTitle => locale.jobs__edit;

  @override
  get viewItemTitle => locale.jobs__view;

  @override
  dispose() {
    super.dispose();
    _name.dispose();
    _profileReadOnly.dispose();
  }

  void _loadProfiles() {
    _profilesLoaded = false;
    _allProfiles.clear();
    context.read<BreadlyDatabase>().profilesDao.getAll().then((profiles) {
      setState(() {
        _allProfiles.clear();
        _allProfiles.addAll(profiles);
        _profilesLoaded = true;

        if (item != null) {
          _updateSelectedProfile(fromItem: true);
          _profileReadOnly.text = _generateProfileCaption(_selectedProfile,
              fallbackCaption: item!.profile.name.value);
        }
      });
    });
  }

  @override
  void initState() {
    super.initState();

    _loadProfiles();
  }

  void _updateAutoName() {
    if (_autoName == _name.text || _name.text.isEmpty) {
      final profileLatestEnd = _schedule.add(_selectedProfileMaxDuration);
      _autoName = sprintf(locale.jobs__autoname_template, [
        _selectedProfile?.name ?? locale.jobs__autoname_stub,
        DateFormat.MMMd(locale.localeName).format(profileLatestEnd),
        DateFormat.jm(locale.localeName).format(profileLatestEnd)
      ]);
      _name.text = _autoName;
    }
  }

  String _generateProfileCaption(final BreadlyProfile? profile,
      {final String? fallbackCaption}) {
    if (profile?.name == null)
      return fallbackCaption ?? locale.profiles__unnamed_profile;

    return '${profile!.name!} (${profile.totalDuration.toLocalizedString(locale)})';
  }

  void _generateProfilesItems(
      final List<DropdownMenuItem<BreadlyProfile?>> profilesItems) {
    profilesItems.clear();
    if (!_profilesLoaded) {
      profilesItems.add(DropdownMenuItem(
        child: CircularProgressIndicator(),
        value: null,
      ));
    } else {
      for (final profile in _allProfiles) {
        profilesItems.add(DropdownMenuItem(
          child: Text(_generateProfileCaption(profile)),
          value: profile,
        ));
      }
      profilesItems.add(DropdownMenuItem(
        child: Text(locale.profiles__new),
        value: _createNewProfile,
      ));
    }
  }

  Widget _buildProfilesField() {
    final inputDecoration = InputDecoration(
      labelText: locale.jobs__profile_label,
    );

    if (!editMode) {
      return TextFormField(
        controller: _profileReadOnly,
        readOnly: true,
        decoration: inputDecoration,
      );
    } else {
      final dropdownState = GlobalKey<FormFieldState>();
      final profilesItems = <DropdownMenuItem<BreadlyProfile?>>[];
      _generateProfilesItems(profilesItems);

      return DropdownButtonFormField<BreadlyProfile?>(
          key: dropdownState,
          decoration: inputDecoration,
          validator: (value) => (value?.profile.id.present ?? false)
              ? null
              : locale.jobs__profile_missing,
          items: profilesItems,
          value: _selectedProfile,
          onChanged: !_profilesLoaded
              ? null
              : (value) {
                  if (value != null && !value.profile.id.present) {
                    Navigator.of(context)
                        .push(MaterialPageRoute(
                            builder: (context) => ProfileDetailsScreen(
                                  profile: null,
                                  editMode: true,
                                )))
                        .then((value) {
                      setState(() {
                        if (value != null) {
                          _allProfiles.add(value);
                          _selectedProfile = value;
                          _generateProfilesItems(profilesItems);
                        } else {
                          _updateSelectedProfile(fromItem: false);
                        }
                        dropdownState.currentState?.didChange(_selectedProfile);
                        _updateAutoName();
                      });
                    });
                  } else {
                    setState(() {
                      _selectedProfile = value;
                      _updateAutoName();
                    });
                  }
                });
    }
  }

  get _selectedProfileMaxDuration => Duration(
      minutes: _selectedProfile?.totalDuration.maxDuration ??
          _selectedProfile?.totalDuration.duration ??
          0);

  Widget _buildScheduleField(final bool scheduleStart) {
    final String scheduleLabel;
    if (_selectedProfile?.totalDuration.maxDuration != null) {
      scheduleLabel = scheduleStart
          ? locale.jobs__start_by_flexible
          : locale.jobs__finish_by_flexible;
    } else {
      scheduleLabel =
          scheduleStart ? locale.jobs__start_by : locale.jobs__finish_by;
    }

    final startEndDiffernce =
        scheduleStart ? Duration.zero : _selectedProfileMaxDuration;

    DateTime minTime = DateTime.now().add(startEndDiffernce);
    if (editMode && _schedule.isBefore(DateTime.now()))
      _schedule = DateTime.now();

    final displaySchedule = _schedule.add(startEndDiffernce);

    final formatter = displaySchedule.isOnSameDay(DateTime.now())
        ? DateFormat.jm(locale.localeName)
        : DateFormat.yMMMd(locale.localeName).add_jm();

    return TextButton(
        child: InputDecorator(
          decoration: InputDecoration(labelText: scheduleLabel),
          child: Text(formatter.format(displaySchedule)),
        ),
        onPressed: !editMode
            ? null
            : () {
                var date = displaySchedule;
                showDialog(
                  context: context,
                  builder: simpleAlertDialogBuilder(
                      scheduleLabel,
                      Container(
                        height: MediaQuery.of(context).size.height / 5,
                        child: CupertinoTheme(
                          data: CupertinoThemeData(
                            textTheme: CupertinoTextThemeData(
                              dateTimePickerTextStyle:
                                  Theme.of(context).textTheme.bodyText1,
                            ),
                          ),
                          child: CupertinoDatePicker(
                            mode: CupertinoDatePickerMode.dateAndTime,
                            use24hFormat: true,
                            minimumDate: minTime,
                            initialDateTime: displaySchedule,
                            onDateTimeChanged: (value) {
                              date = value;
                            },
                          ),
                        ),
                      ),
                      AlertDialogButton.getOkCancelButtons(context)),
                ).then((result) {
                  if (result) {
                    setState(() {
                      _schedule = date.subtract(startEndDiffernce);
                      _updateAutoName();
                    });
                  }
                });
              });
  }

  @override
  buildForm() {
    return [
      buildTextField(
        // Name field
        locale.jobs__name_label,
        _name,
        autofocus: true,
        validator: valueRequired(locale.jobs__name_missing),
      ),
      _buildProfilesField(),
      Row(children: [
        Expanded(child: _buildScheduleField(true)),
        Expanded(child: _buildScheduleField(false)),
      ]),
    ];
  }

  @override
  checkDeleteAllowed() => showDialog(
        context: context,
        builder: textAlertDialogBuilder(
          locale.jobs__delete_caption,
          locale.jobs__delete_message,
          AlertDialogButton.getDangerConfirmButtons(context),
        ),
      ).then((result) => result == true);

  @override
  Future<bool> checkEditAllowed() =>
      Future.value(true); // TODO warn about lost changed / overrides

  @override
  deleteItem() => item == null
      ? Future.value(true)
      : context.read<BreadlyDatabase>().jobsDao.remove(item!);

  @override
  BreadlyJob generateItemFromModel() {
    BreadlyJob newJob = item ?? BreadlyJob();
    newJob.apply(
      name: _name.text,
      currentStepIndex: -1,
      currentStepStart: _schedule,
    );
    if (_selectedProfile != null) {
      newJob.profile = _selectedProfile!.profile;
      newJob.addSteps(_selectedProfile!.steps);
    }
    return newJob;
  }

  void _updateSelectedProfile({required bool fromItem}) {
    if (_profilesLoaded) {
      final profileId;
      if (!fromItem && (_selectedProfile?.profile.id.present ?? false))
        profileId = _selectedProfile!.profile.id.value;
      else if (item?.profile.id.present ?? false)
        profileId = item!.profile.id.value;
      else
        profileId = null;

      _selectedProfile = _allProfiles.firstWhereOrNull((profile) =>
          profile.profile.id.present && profile.profile.id.value == profileId);
    }
  }

  @override
  refreshModelFromItem() {
    _name.text = item?.name ?? '';
    _schedule = item?.currentStepStart ?? DateTime.now();
    _updateSelectedProfile(fromItem: true);
  }

  @override
  saveItem() => item == null
      ? Future.value(false)
      : context.read<BreadlyDatabase>().jobsDao.save(item!);
}
