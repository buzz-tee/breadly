import 'package:breadly/model/model.dart';
import 'package:breadly/pages/job_list.dart';
import 'package:breadly/pages/profile_list.dart';
import 'package:breadly/pages/routes.dart';
import 'package:breadly/widgets/drawer.dart';
import 'package:breadly/widgets/list_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(
    Provider<BreadlyDatabase>(
      create: (context) => BreadlyDatabase(),
      child: BreadlyApp(),
      dispose: (context, db) => db.close(),
    ),
  );
}

class BreadlyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Breadly - Baking',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      home: BreadlyMainScreen(),
    );
  }
}

class BreadlyMainScreen extends StatefulWidget {
  @override
  _BreadlyMainScreenState createState() => _BreadlyMainScreenState();
}

class _BreadlyMainScreenState extends State<BreadlyMainScreen> {
  Section _currentSection = Section.JOBS;

  void _onNavSection(Section section) {
    setState(() {
      this._currentSection = section;
    });
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    final CustomListScreen home;

    switch (_currentSection) {
      case Section.JOBS:
        home = JobListScreen();
        break;
      case Section.PROFILES:
        home = ProfileListScreen();
        break;
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.app_title +
            ' - ' +
            home.getTitle(context)),
      ),
      body: home,
      drawer: AppDrawer(onNavSection: _onNavSection),
      floatingActionButton: home.floatingActionButton,
    );
  }
}
