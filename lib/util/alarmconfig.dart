import 'package:breadly/model/model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

extension AlarmConfigUi on AlarmConfig {
  String getText(final AppLocalizations locale) {
    switch (this) {
      case AlarmConfig.AlarmOn:
        return 'Alarm enabled';
      case AlarmConfig.AlarmOnAutoAdvance:
        return 'Alarm enabled (auto-advance)';
      case AlarmConfig.AlarmOff:
        return 'Alarm disabled';
    }
  }

  Icon getIcon({final Color? defaultColor}) {
    switch (this) {
      case AlarmConfig.AlarmOn:
        return Icon(Icons.alarm_outlined, color: Colors.red);
      case AlarmConfig.AlarmOnAutoAdvance:
        return Icon(Icons.alarm_outlined, color: defaultColor);
      case AlarmConfig.AlarmOff:
        return Icon(Icons.alarm_off_outlined, color: defaultColor);
    }
  }
}
