extension DateOnlyComparison on DateTime {
  bool isOnSameDay(DateTime other) =>
      year == other.year && month == other.month && day == other.day;
}
