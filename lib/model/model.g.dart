// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'model.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class DbJob extends DataClass implements Insertable<DbJob> {
  final int id;
  final String name;
  final int profileId;
  final int currentStepIndex;
  final DateTime currentStepStart;
  DbJob(
      {required this.id,
      required this.name,
      required this.profileId,
      required this.currentStepIndex,
      required this.currentStepStart});
  factory DbJob.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return DbJob(
      id: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      name: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}name'])!,
      profileId: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}profile_id'])!,
      currentStepIndex: const IntType().mapFromDatabaseResponse(
          data['${effectivePrefix}current_step_index'])!,
      currentStepStart: const DateTimeType().mapFromDatabaseResponse(
          data['${effectivePrefix}current_step_start'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['name'] = Variable<String>(name);
    map['profile_id'] = Variable<int>(profileId);
    map['current_step_index'] = Variable<int>(currentStepIndex);
    map['current_step_start'] = Variable<DateTime>(currentStepStart);
    return map;
  }

  DbJobsCompanion toCompanion(bool nullToAbsent) {
    return DbJobsCompanion(
      id: Value(id),
      name: Value(name),
      profileId: Value(profileId),
      currentStepIndex: Value(currentStepIndex),
      currentStepStart: Value(currentStepStart),
    );
  }

  factory DbJob.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return DbJob(
      id: serializer.fromJson<int>(json['id']),
      name: serializer.fromJson<String>(json['name']),
      profileId: serializer.fromJson<int>(json['profileId']),
      currentStepIndex: serializer.fromJson<int>(json['currentStepIndex']),
      currentStepStart: serializer.fromJson<DateTime>(json['currentStepStart']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'name': serializer.toJson<String>(name),
      'profileId': serializer.toJson<int>(profileId),
      'currentStepIndex': serializer.toJson<int>(currentStepIndex),
      'currentStepStart': serializer.toJson<DateTime>(currentStepStart),
    };
  }

  DbJob copyWith(
          {int? id,
          String? name,
          int? profileId,
          int? currentStepIndex,
          DateTime? currentStepStart}) =>
      DbJob(
        id: id ?? this.id,
        name: name ?? this.name,
        profileId: profileId ?? this.profileId,
        currentStepIndex: currentStepIndex ?? this.currentStepIndex,
        currentStepStart: currentStepStart ?? this.currentStepStart,
      );
  @override
  String toString() {
    return (StringBuffer('DbJob(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('profileId: $profileId, ')
          ..write('currentStepIndex: $currentStepIndex, ')
          ..write('currentStepStart: $currentStepStart')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          name.hashCode,
          $mrjc(profileId.hashCode,
              $mrjc(currentStepIndex.hashCode, currentStepStart.hashCode)))));
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is DbJob &&
          other.id == this.id &&
          other.name == this.name &&
          other.profileId == this.profileId &&
          other.currentStepIndex == this.currentStepIndex &&
          other.currentStepStart == this.currentStepStart);
}

class DbJobsCompanion extends UpdateCompanion<DbJob> {
  final Value<int> id;
  final Value<String> name;
  final Value<int> profileId;
  final Value<int> currentStepIndex;
  final Value<DateTime> currentStepStart;
  const DbJobsCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.profileId = const Value.absent(),
    this.currentStepIndex = const Value.absent(),
    this.currentStepStart = const Value.absent(),
  });
  DbJobsCompanion.insert({
    this.id = const Value.absent(),
    required String name,
    required int profileId,
    this.currentStepIndex = const Value.absent(),
    this.currentStepStart = const Value.absent(),
  })  : name = Value(name),
        profileId = Value(profileId);
  static Insertable<DbJob> custom({
    Expression<int>? id,
    Expression<String>? name,
    Expression<int>? profileId,
    Expression<int>? currentStepIndex,
    Expression<DateTime>? currentStepStart,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (name != null) 'name': name,
      if (profileId != null) 'profile_id': profileId,
      if (currentStepIndex != null) 'current_step_index': currentStepIndex,
      if (currentStepStart != null) 'current_step_start': currentStepStart,
    });
  }

  DbJobsCompanion copyWith(
      {Value<int>? id,
      Value<String>? name,
      Value<int>? profileId,
      Value<int>? currentStepIndex,
      Value<DateTime>? currentStepStart}) {
    return DbJobsCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
      profileId: profileId ?? this.profileId,
      currentStepIndex: currentStepIndex ?? this.currentStepIndex,
      currentStepStart: currentStepStart ?? this.currentStepStart,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (profileId.present) {
      map['profile_id'] = Variable<int>(profileId.value);
    }
    if (currentStepIndex.present) {
      map['current_step_index'] = Variable<int>(currentStepIndex.value);
    }
    if (currentStepStart.present) {
      map['current_step_start'] = Variable<DateTime>(currentStepStart.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DbJobsCompanion(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('profileId: $profileId, ')
          ..write('currentStepIndex: $currentStepIndex, ')
          ..write('currentStepStart: $currentStepStart')
          ..write(')'))
        .toString();
  }
}

class $DbJobsTable extends DbJobs with TableInfo<$DbJobsTable, DbJob> {
  final GeneratedDatabase _db;
  final String? _alias;
  $DbJobsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedIntColumn id = _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedTextColumn name = _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn(
      'name',
      $tableName,
      false,
    );
  }

  final VerificationMeta _profileIdMeta = const VerificationMeta('profileId');
  @override
  late final GeneratedIntColumn profileId = _constructProfileId();
  GeneratedIntColumn _constructProfileId() {
    return GeneratedIntColumn(
      'profile_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _currentStepIndexMeta =
      const VerificationMeta('currentStepIndex');
  @override
  late final GeneratedIntColumn currentStepIndex = _constructCurrentStepIndex();
  GeneratedIntColumn _constructCurrentStepIndex() {
    return GeneratedIntColumn('current_step_index', $tableName, false,
        defaultValue: const Constant(-1));
  }

  final VerificationMeta _currentStepStartMeta =
      const VerificationMeta('currentStepStart');
  @override
  late final GeneratedDateTimeColumn currentStepStart =
      _constructCurrentStepStart();
  GeneratedDateTimeColumn _constructCurrentStepStart() {
    return GeneratedDateTimeColumn('current_step_start', $tableName, false,
        defaultValue: currentDateAndTime);
  }

  @override
  List<GeneratedColumn> get $columns =>
      [id, name, profileId, currentStepIndex, currentStepStart];
  @override
  $DbJobsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'jobs';
  @override
  final String actualTableName = 'jobs';
  @override
  VerificationContext validateIntegrity(Insertable<DbJob> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('profile_id')) {
      context.handle(_profileIdMeta,
          profileId.isAcceptableOrUnknown(data['profile_id']!, _profileIdMeta));
    } else if (isInserting) {
      context.missing(_profileIdMeta);
    }
    if (data.containsKey('current_step_index')) {
      context.handle(
          _currentStepIndexMeta,
          currentStepIndex.isAcceptableOrUnknown(
              data['current_step_index']!, _currentStepIndexMeta));
    }
    if (data.containsKey('current_step_start')) {
      context.handle(
          _currentStepStartMeta,
          currentStepStart.isAcceptableOrUnknown(
              data['current_step_start']!, _currentStepStartMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  DbJob map(Map<String, dynamic> data, {String? tablePrefix}) {
    return DbJob.fromData(data, _db,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $DbJobsTable createAlias(String alias) {
    return $DbJobsTable(_db, alias);
  }
}

class DbJobStep extends DataClass implements Insertable<DbJobStep> {
  final int jobId;
  final int index;
  final String name;
  final int duration;
  final int? maxDuration;
  final AlarmConfig alarmConfig;
  DbJobStep(
      {required this.jobId,
      required this.index,
      required this.name,
      required this.duration,
      this.maxDuration,
      required this.alarmConfig});
  factory DbJobStep.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return DbJobStep(
      jobId: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}job_id'])!,
      index: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}index'])!,
      name: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}name'])!,
      duration: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}duration'])!,
      maxDuration: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}max_duration']),
      alarmConfig: $DbJobStepsTable.$converter0.mapToDart(const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}alarm_config']))!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['job_id'] = Variable<int>(jobId);
    map['index'] = Variable<int>(index);
    map['name'] = Variable<String>(name);
    map['duration'] = Variable<int>(duration);
    if (!nullToAbsent || maxDuration != null) {
      map['max_duration'] = Variable<int?>(maxDuration);
    }
    {
      final converter = $DbJobStepsTable.$converter0;
      map['alarm_config'] = Variable<int>(converter.mapToSql(alarmConfig)!);
    }
    return map;
  }

  DbJobStepsCompanion toCompanion(bool nullToAbsent) {
    return DbJobStepsCompanion(
      jobId: Value(jobId),
      index: Value(index),
      name: Value(name),
      duration: Value(duration),
      maxDuration: maxDuration == null && nullToAbsent
          ? const Value.absent()
          : Value(maxDuration),
      alarmConfig: Value(alarmConfig),
    );
  }

  factory DbJobStep.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return DbJobStep(
      jobId: serializer.fromJson<int>(json['jobId']),
      index: serializer.fromJson<int>(json['index']),
      name: serializer.fromJson<String>(json['name']),
      duration: serializer.fromJson<int>(json['duration']),
      maxDuration: serializer.fromJson<int?>(json['maxDuration']),
      alarmConfig: serializer.fromJson<AlarmConfig>(json['alarmConfig']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'jobId': serializer.toJson<int>(jobId),
      'index': serializer.toJson<int>(index),
      'name': serializer.toJson<String>(name),
      'duration': serializer.toJson<int>(duration),
      'maxDuration': serializer.toJson<int?>(maxDuration),
      'alarmConfig': serializer.toJson<AlarmConfig>(alarmConfig),
    };
  }

  DbJobStep copyWith(
          {int? jobId,
          int? index,
          String? name,
          int? duration,
          int? maxDuration,
          AlarmConfig? alarmConfig}) =>
      DbJobStep(
        jobId: jobId ?? this.jobId,
        index: index ?? this.index,
        name: name ?? this.name,
        duration: duration ?? this.duration,
        maxDuration: maxDuration ?? this.maxDuration,
        alarmConfig: alarmConfig ?? this.alarmConfig,
      );
  @override
  String toString() {
    return (StringBuffer('DbJobStep(')
          ..write('jobId: $jobId, ')
          ..write('index: $index, ')
          ..write('name: $name, ')
          ..write('duration: $duration, ')
          ..write('maxDuration: $maxDuration, ')
          ..write('alarmConfig: $alarmConfig')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      jobId.hashCode,
      $mrjc(
          index.hashCode,
          $mrjc(
              name.hashCode,
              $mrjc(duration.hashCode,
                  $mrjc(maxDuration.hashCode, alarmConfig.hashCode))))));
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is DbJobStep &&
          other.jobId == this.jobId &&
          other.index == this.index &&
          other.name == this.name &&
          other.duration == this.duration &&
          other.maxDuration == this.maxDuration &&
          other.alarmConfig == this.alarmConfig);
}

class DbJobStepsCompanion extends UpdateCompanion<DbJobStep> {
  final Value<int> jobId;
  final Value<int> index;
  final Value<String> name;
  final Value<int> duration;
  final Value<int?> maxDuration;
  final Value<AlarmConfig> alarmConfig;
  const DbJobStepsCompanion({
    this.jobId = const Value.absent(),
    this.index = const Value.absent(),
    this.name = const Value.absent(),
    this.duration = const Value.absent(),
    this.maxDuration = const Value.absent(),
    this.alarmConfig = const Value.absent(),
  });
  DbJobStepsCompanion.insert({
    required int jobId,
    required int index,
    required String name,
    required int duration,
    this.maxDuration = const Value.absent(),
    required AlarmConfig alarmConfig,
  })  : jobId = Value(jobId),
        index = Value(index),
        name = Value(name),
        duration = Value(duration),
        alarmConfig = Value(alarmConfig);
  static Insertable<DbJobStep> custom({
    Expression<int>? jobId,
    Expression<int>? index,
    Expression<String>? name,
    Expression<int>? duration,
    Expression<int?>? maxDuration,
    Expression<AlarmConfig>? alarmConfig,
  }) {
    return RawValuesInsertable({
      if (jobId != null) 'job_id': jobId,
      if (index != null) 'index': index,
      if (name != null) 'name': name,
      if (duration != null) 'duration': duration,
      if (maxDuration != null) 'max_duration': maxDuration,
      if (alarmConfig != null) 'alarm_config': alarmConfig,
    });
  }

  DbJobStepsCompanion copyWith(
      {Value<int>? jobId,
      Value<int>? index,
      Value<String>? name,
      Value<int>? duration,
      Value<int?>? maxDuration,
      Value<AlarmConfig>? alarmConfig}) {
    return DbJobStepsCompanion(
      jobId: jobId ?? this.jobId,
      index: index ?? this.index,
      name: name ?? this.name,
      duration: duration ?? this.duration,
      maxDuration: maxDuration ?? this.maxDuration,
      alarmConfig: alarmConfig ?? this.alarmConfig,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (jobId.present) {
      map['job_id'] = Variable<int>(jobId.value);
    }
    if (index.present) {
      map['index'] = Variable<int>(index.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (duration.present) {
      map['duration'] = Variable<int>(duration.value);
    }
    if (maxDuration.present) {
      map['max_duration'] = Variable<int?>(maxDuration.value);
    }
    if (alarmConfig.present) {
      final converter = $DbJobStepsTable.$converter0;
      map['alarm_config'] =
          Variable<int>(converter.mapToSql(alarmConfig.value)!);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DbJobStepsCompanion(')
          ..write('jobId: $jobId, ')
          ..write('index: $index, ')
          ..write('name: $name, ')
          ..write('duration: $duration, ')
          ..write('maxDuration: $maxDuration, ')
          ..write('alarmConfig: $alarmConfig')
          ..write(')'))
        .toString();
  }
}

class $DbJobStepsTable extends DbJobSteps
    with TableInfo<$DbJobStepsTable, DbJobStep> {
  final GeneratedDatabase _db;
  final String? _alias;
  $DbJobStepsTable(this._db, [this._alias]);
  final VerificationMeta _jobIdMeta = const VerificationMeta('jobId');
  @override
  late final GeneratedIntColumn jobId = _constructJobId();
  GeneratedIntColumn _constructJobId() {
    return GeneratedIntColumn(
      'job_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _indexMeta = const VerificationMeta('index');
  @override
  late final GeneratedIntColumn index = _constructIndex();
  GeneratedIntColumn _constructIndex() {
    return GeneratedIntColumn(
      'index',
      $tableName,
      false,
    );
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedTextColumn name = _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn(
      'name',
      $tableName,
      false,
    );
  }

  final VerificationMeta _durationMeta = const VerificationMeta('duration');
  @override
  late final GeneratedIntColumn duration = _constructDuration();
  GeneratedIntColumn _constructDuration() {
    return GeneratedIntColumn(
      'duration',
      $tableName,
      false,
    );
  }

  final VerificationMeta _maxDurationMeta =
      const VerificationMeta('maxDuration');
  @override
  late final GeneratedIntColumn maxDuration = _constructMaxDuration();
  GeneratedIntColumn _constructMaxDuration() {
    return GeneratedIntColumn(
      'max_duration',
      $tableName,
      true,
    );
  }

  final VerificationMeta _alarmConfigMeta =
      const VerificationMeta('alarmConfig');
  @override
  late final GeneratedIntColumn alarmConfig = _constructAlarmConfig();
  GeneratedIntColumn _constructAlarmConfig() {
    return GeneratedIntColumn(
      'alarm_config',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns =>
      [jobId, index, name, duration, maxDuration, alarmConfig];
  @override
  $DbJobStepsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'job_steps';
  @override
  final String actualTableName = 'job_steps';
  @override
  VerificationContext validateIntegrity(Insertable<DbJobStep> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('job_id')) {
      context.handle(
          _jobIdMeta, jobId.isAcceptableOrUnknown(data['job_id']!, _jobIdMeta));
    } else if (isInserting) {
      context.missing(_jobIdMeta);
    }
    if (data.containsKey('index')) {
      context.handle(
          _indexMeta, index.isAcceptableOrUnknown(data['index']!, _indexMeta));
    } else if (isInserting) {
      context.missing(_indexMeta);
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('duration')) {
      context.handle(_durationMeta,
          duration.isAcceptableOrUnknown(data['duration']!, _durationMeta));
    } else if (isInserting) {
      context.missing(_durationMeta);
    }
    if (data.containsKey('max_duration')) {
      context.handle(
          _maxDurationMeta,
          maxDuration.isAcceptableOrUnknown(
              data['max_duration']!, _maxDurationMeta));
    }
    context.handle(_alarmConfigMeta, const VerificationResult.success());
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  DbJobStep map(Map<String, dynamic> data, {String? tablePrefix}) {
    return DbJobStep.fromData(data, _db,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $DbJobStepsTable createAlias(String alias) {
    return $DbJobStepsTable(_db, alias);
  }

  static TypeConverter<AlarmConfig, int> $converter0 =
      const EnumIndexConverter<AlarmConfig>(AlarmConfig.values);
}

class DbProfile extends DataClass implements Insertable<DbProfile> {
  final int id;
  final String name;
  final String? description;
  DbProfile({required this.id, required this.name, this.description});
  factory DbProfile.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return DbProfile(
      id: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      name: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}name'])!,
      description: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}description']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['name'] = Variable<String>(name);
    if (!nullToAbsent || description != null) {
      map['description'] = Variable<String?>(description);
    }
    return map;
  }

  DbProfilesCompanion toCompanion(bool nullToAbsent) {
    return DbProfilesCompanion(
      id: Value(id),
      name: Value(name),
      description: description == null && nullToAbsent
          ? const Value.absent()
          : Value(description),
    );
  }

  factory DbProfile.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return DbProfile(
      id: serializer.fromJson<int>(json['id']),
      name: serializer.fromJson<String>(json['name']),
      description: serializer.fromJson<String?>(json['description']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'name': serializer.toJson<String>(name),
      'description': serializer.toJson<String?>(description),
    };
  }

  DbProfile copyWith({int? id, String? name, String? description}) => DbProfile(
        id: id ?? this.id,
        name: name ?? this.name,
        description: description ?? this.description,
      );
  @override
  String toString() {
    return (StringBuffer('DbProfile(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('description: $description')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      $mrjf($mrjc(id.hashCode, $mrjc(name.hashCode, description.hashCode)));
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is DbProfile &&
          other.id == this.id &&
          other.name == this.name &&
          other.description == this.description);
}

class DbProfilesCompanion extends UpdateCompanion<DbProfile> {
  final Value<int> id;
  final Value<String> name;
  final Value<String?> description;
  const DbProfilesCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.description = const Value.absent(),
  });
  DbProfilesCompanion.insert({
    this.id = const Value.absent(),
    required String name,
    this.description = const Value.absent(),
  }) : name = Value(name);
  static Insertable<DbProfile> custom({
    Expression<int>? id,
    Expression<String>? name,
    Expression<String?>? description,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (name != null) 'name': name,
      if (description != null) 'description': description,
    });
  }

  DbProfilesCompanion copyWith(
      {Value<int>? id, Value<String>? name, Value<String?>? description}) {
    return DbProfilesCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
      description: description ?? this.description,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (description.present) {
      map['description'] = Variable<String?>(description.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DbProfilesCompanion(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('description: $description')
          ..write(')'))
        .toString();
  }
}

class $DbProfilesTable extends DbProfiles
    with TableInfo<$DbProfilesTable, DbProfile> {
  final GeneratedDatabase _db;
  final String? _alias;
  $DbProfilesTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedIntColumn id = _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedTextColumn name = _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn(
      'name',
      $tableName,
      false,
    );
  }

  final VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  @override
  late final GeneratedTextColumn description = _constructDescription();
  GeneratedTextColumn _constructDescription() {
    return GeneratedTextColumn(
      'description',
      $tableName,
      true,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, name, description];
  @override
  $DbProfilesTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'profiles';
  @override
  final String actualTableName = 'profiles';
  @override
  VerificationContext validateIntegrity(Insertable<DbProfile> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description']!, _descriptionMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  DbProfile map(Map<String, dynamic> data, {String? tablePrefix}) {
    return DbProfile.fromData(data, _db,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $DbProfilesTable createAlias(String alias) {
    return $DbProfilesTable(_db, alias);
  }
}

class DbProfileStep extends DataClass implements Insertable<DbProfileStep> {
  final int profileId;
  final int index;
  final String name;
  final int duration;
  final int? maxDuration;
  final AlarmConfig alarmConfig;
  DbProfileStep(
      {required this.profileId,
      required this.index,
      required this.name,
      required this.duration,
      this.maxDuration,
      required this.alarmConfig});
  factory DbProfileStep.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return DbProfileStep(
      profileId: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}profile_id'])!,
      index: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}index'])!,
      name: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}name'])!,
      duration: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}duration'])!,
      maxDuration: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}max_duration']),
      alarmConfig: $DbProfileStepsTable.$converter0.mapToDart(const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}alarm_config']))!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['profile_id'] = Variable<int>(profileId);
    map['index'] = Variable<int>(index);
    map['name'] = Variable<String>(name);
    map['duration'] = Variable<int>(duration);
    if (!nullToAbsent || maxDuration != null) {
      map['max_duration'] = Variable<int?>(maxDuration);
    }
    {
      final converter = $DbProfileStepsTable.$converter0;
      map['alarm_config'] = Variable<int>(converter.mapToSql(alarmConfig)!);
    }
    return map;
  }

  DbProfileStepsCompanion toCompanion(bool nullToAbsent) {
    return DbProfileStepsCompanion(
      profileId: Value(profileId),
      index: Value(index),
      name: Value(name),
      duration: Value(duration),
      maxDuration: maxDuration == null && nullToAbsent
          ? const Value.absent()
          : Value(maxDuration),
      alarmConfig: Value(alarmConfig),
    );
  }

  factory DbProfileStep.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return DbProfileStep(
      profileId: serializer.fromJson<int>(json['profileId']),
      index: serializer.fromJson<int>(json['index']),
      name: serializer.fromJson<String>(json['name']),
      duration: serializer.fromJson<int>(json['duration']),
      maxDuration: serializer.fromJson<int?>(json['maxDuration']),
      alarmConfig: serializer.fromJson<AlarmConfig>(json['alarmConfig']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'profileId': serializer.toJson<int>(profileId),
      'index': serializer.toJson<int>(index),
      'name': serializer.toJson<String>(name),
      'duration': serializer.toJson<int>(duration),
      'maxDuration': serializer.toJson<int?>(maxDuration),
      'alarmConfig': serializer.toJson<AlarmConfig>(alarmConfig),
    };
  }

  DbProfileStep copyWith(
          {int? profileId,
          int? index,
          String? name,
          int? duration,
          int? maxDuration,
          AlarmConfig? alarmConfig}) =>
      DbProfileStep(
        profileId: profileId ?? this.profileId,
        index: index ?? this.index,
        name: name ?? this.name,
        duration: duration ?? this.duration,
        maxDuration: maxDuration ?? this.maxDuration,
        alarmConfig: alarmConfig ?? this.alarmConfig,
      );
  @override
  String toString() {
    return (StringBuffer('DbProfileStep(')
          ..write('profileId: $profileId, ')
          ..write('index: $index, ')
          ..write('name: $name, ')
          ..write('duration: $duration, ')
          ..write('maxDuration: $maxDuration, ')
          ..write('alarmConfig: $alarmConfig')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      profileId.hashCode,
      $mrjc(
          index.hashCode,
          $mrjc(
              name.hashCode,
              $mrjc(duration.hashCode,
                  $mrjc(maxDuration.hashCode, alarmConfig.hashCode))))));
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is DbProfileStep &&
          other.profileId == this.profileId &&
          other.index == this.index &&
          other.name == this.name &&
          other.duration == this.duration &&
          other.maxDuration == this.maxDuration &&
          other.alarmConfig == this.alarmConfig);
}

class DbProfileStepsCompanion extends UpdateCompanion<DbProfileStep> {
  final Value<int> profileId;
  final Value<int> index;
  final Value<String> name;
  final Value<int> duration;
  final Value<int?> maxDuration;
  final Value<AlarmConfig> alarmConfig;
  const DbProfileStepsCompanion({
    this.profileId = const Value.absent(),
    this.index = const Value.absent(),
    this.name = const Value.absent(),
    this.duration = const Value.absent(),
    this.maxDuration = const Value.absent(),
    this.alarmConfig = const Value.absent(),
  });
  DbProfileStepsCompanion.insert({
    required int profileId,
    required int index,
    required String name,
    required int duration,
    this.maxDuration = const Value.absent(),
    required AlarmConfig alarmConfig,
  })  : profileId = Value(profileId),
        index = Value(index),
        name = Value(name),
        duration = Value(duration),
        alarmConfig = Value(alarmConfig);
  static Insertable<DbProfileStep> custom({
    Expression<int>? profileId,
    Expression<int>? index,
    Expression<String>? name,
    Expression<int>? duration,
    Expression<int?>? maxDuration,
    Expression<AlarmConfig>? alarmConfig,
  }) {
    return RawValuesInsertable({
      if (profileId != null) 'profile_id': profileId,
      if (index != null) 'index': index,
      if (name != null) 'name': name,
      if (duration != null) 'duration': duration,
      if (maxDuration != null) 'max_duration': maxDuration,
      if (alarmConfig != null) 'alarm_config': alarmConfig,
    });
  }

  DbProfileStepsCompanion copyWith(
      {Value<int>? profileId,
      Value<int>? index,
      Value<String>? name,
      Value<int>? duration,
      Value<int?>? maxDuration,
      Value<AlarmConfig>? alarmConfig}) {
    return DbProfileStepsCompanion(
      profileId: profileId ?? this.profileId,
      index: index ?? this.index,
      name: name ?? this.name,
      duration: duration ?? this.duration,
      maxDuration: maxDuration ?? this.maxDuration,
      alarmConfig: alarmConfig ?? this.alarmConfig,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (profileId.present) {
      map['profile_id'] = Variable<int>(profileId.value);
    }
    if (index.present) {
      map['index'] = Variable<int>(index.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (duration.present) {
      map['duration'] = Variable<int>(duration.value);
    }
    if (maxDuration.present) {
      map['max_duration'] = Variable<int?>(maxDuration.value);
    }
    if (alarmConfig.present) {
      final converter = $DbProfileStepsTable.$converter0;
      map['alarm_config'] =
          Variable<int>(converter.mapToSql(alarmConfig.value)!);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DbProfileStepsCompanion(')
          ..write('profileId: $profileId, ')
          ..write('index: $index, ')
          ..write('name: $name, ')
          ..write('duration: $duration, ')
          ..write('maxDuration: $maxDuration, ')
          ..write('alarmConfig: $alarmConfig')
          ..write(')'))
        .toString();
  }
}

class $DbProfileStepsTable extends DbProfileSteps
    with TableInfo<$DbProfileStepsTable, DbProfileStep> {
  final GeneratedDatabase _db;
  final String? _alias;
  $DbProfileStepsTable(this._db, [this._alias]);
  final VerificationMeta _profileIdMeta = const VerificationMeta('profileId');
  @override
  late final GeneratedIntColumn profileId = _constructProfileId();
  GeneratedIntColumn _constructProfileId() {
    return GeneratedIntColumn(
      'profile_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _indexMeta = const VerificationMeta('index');
  @override
  late final GeneratedIntColumn index = _constructIndex();
  GeneratedIntColumn _constructIndex() {
    return GeneratedIntColumn(
      'index',
      $tableName,
      false,
    );
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedTextColumn name = _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn(
      'name',
      $tableName,
      false,
    );
  }

  final VerificationMeta _durationMeta = const VerificationMeta('duration');
  @override
  late final GeneratedIntColumn duration = _constructDuration();
  GeneratedIntColumn _constructDuration() {
    return GeneratedIntColumn(
      'duration',
      $tableName,
      false,
    );
  }

  final VerificationMeta _maxDurationMeta =
      const VerificationMeta('maxDuration');
  @override
  late final GeneratedIntColumn maxDuration = _constructMaxDuration();
  GeneratedIntColumn _constructMaxDuration() {
    return GeneratedIntColumn(
      'max_duration',
      $tableName,
      true,
    );
  }

  final VerificationMeta _alarmConfigMeta =
      const VerificationMeta('alarmConfig');
  @override
  late final GeneratedIntColumn alarmConfig = _constructAlarmConfig();
  GeneratedIntColumn _constructAlarmConfig() {
    return GeneratedIntColumn(
      'alarm_config',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns =>
      [profileId, index, name, duration, maxDuration, alarmConfig];
  @override
  $DbProfileStepsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'profile_steps';
  @override
  final String actualTableName = 'profile_steps';
  @override
  VerificationContext validateIntegrity(Insertable<DbProfileStep> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('profile_id')) {
      context.handle(_profileIdMeta,
          profileId.isAcceptableOrUnknown(data['profile_id']!, _profileIdMeta));
    } else if (isInserting) {
      context.missing(_profileIdMeta);
    }
    if (data.containsKey('index')) {
      context.handle(
          _indexMeta, index.isAcceptableOrUnknown(data['index']!, _indexMeta));
    } else if (isInserting) {
      context.missing(_indexMeta);
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('duration')) {
      context.handle(_durationMeta,
          duration.isAcceptableOrUnknown(data['duration']!, _durationMeta));
    } else if (isInserting) {
      context.missing(_durationMeta);
    }
    if (data.containsKey('max_duration')) {
      context.handle(
          _maxDurationMeta,
          maxDuration.isAcceptableOrUnknown(
              data['max_duration']!, _maxDurationMeta));
    }
    context.handle(_alarmConfigMeta, const VerificationResult.success());
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  DbProfileStep map(Map<String, dynamic> data, {String? tablePrefix}) {
    return DbProfileStep.fromData(data, _db,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $DbProfileStepsTable createAlias(String alias) {
    return $DbProfileStepsTable(_db, alias);
  }

  static TypeConverter<AlarmConfig, int> $converter0 =
      const EnumIndexConverter<AlarmConfig>(AlarmConfig.values);
}

abstract class _$BreadlyDatabase extends GeneratedDatabase {
  _$BreadlyDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  late final $DbJobsTable dbJobs = $DbJobsTable(this);
  late final $DbJobStepsTable dbJobSteps = $DbJobStepsTable(this);
  late final $DbProfilesTable dbProfiles = $DbProfilesTable(this);
  late final $DbProfileStepsTable dbProfileSteps = $DbProfileStepsTable(this);
  late final JobsDao jobsDao = JobsDao(this as BreadlyDatabase);
  late final ProfilesDao profilesDao = ProfilesDao(this as BreadlyDatabase);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities =>
      [dbJobs, dbJobSteps, dbProfiles, dbProfileSteps];
}
