// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profiles_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$ProfilesDaoMixin on DatabaseAccessor<BreadlyDatabase> {
  $DbJobsTable get dbJobs => attachedDatabase.dbJobs;
  $DbProfilesTable get dbProfiles => attachedDatabase.dbProfiles;
  $DbProfileStepsTable get dbProfileSteps => attachedDatabase.dbProfileSteps;
}
