// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'jobs_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$JobsDaoMixin on DatabaseAccessor<BreadlyDatabase> {
  $DbJobsTable get dbJobs => attachedDatabase.dbJobs;
  $DbJobStepsTable get dbJobSteps => attachedDatabase.dbJobSteps;
  $DbProfilesTable get dbProfiles => attachedDatabase.dbProfiles;
}
