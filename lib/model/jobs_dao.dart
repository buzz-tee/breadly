import 'package:breadly/model/model.dart';
import 'package:moor/moor.dart';

part 'jobs_dao.g.dart';

@UseDao(tables: [DbJobs, DbJobSteps, DbProfiles])
class JobsDao extends DatabaseAccessor<BreadlyDatabase> with _$JobsDaoMixin {
  JobsDao(final BreadlyDatabase db) : super(db);

  void _deleteJobSteps(final BreadlyJob job) {
    if (job.job.id.present) {
      (delete(dbJobSteps)..where((s) => s.jobId.equals(job.job.id.value))).go();
    }
  }

  void _insertJobSteps(final BreadlyJob job) {
    job.steps.forEach((step) {
      step.jobId = job.job.id.value;
      into(dbJobSteps).insert(step.step);
    });
  }

  Future<List<BreadlyJob>> getAll() {
    return (select(dbJobs).join([
      leftOuterJoin(dbProfiles, dbJobs.profileId.equalsExp(dbProfiles.id)),
      leftOuterJoin(dbJobSteps, dbJobSteps.jobId.equalsExp(dbJobs.id)),
    ])
          ..orderBy([OrderingTerm.asc(dbJobSteps.index)]))
        .get()
        .then((rows) {
      final jobsToProfiles = <DbJob, DbProfile>{};
      final jobsToSteps = <DbJob, List<BreadlyJobStep>>{};
      for (final row in rows) {
        final job = row.readTable(dbJobs);
        final profile = row.readTable(dbProfiles);
        final jobStep = row.readTableOrNull(dbJobSteps);

        jobsToProfiles[job] = profile;
        final jobStepList = jobsToSteps.putIfAbsent(job, () => []);
        if (jobStep != null)
          jobStepList.add(BreadlyJobStep(step: jobStep.toCompanion(true)));
      }

      return [
        for (final jobEntry in jobsToSteps.entries.toList(growable: false)
          ..sort((a, b) =>
              a.key.currentStepStart.compareTo(b.key.currentStepStart)))
          BreadlyJob.from(jobEntry.key, jobsToProfiles[jobEntry.key],
              jobsToSteps[jobEntry.key] ?? [])
      ];
    });
  }

  Future<bool> save(final BreadlyJob job) {
    job.job = job.job.copyWith(profileId: job.profile.id);
    if (job.job.id.present) {
      _deleteJobSteps(job);
      _insertJobSteps(job);

      return (update(dbJobs)..where((j) => j.id.equals(job.job.id.value)))
          .write(job.job)
          .then((value) => value > 0)
          .catchError((_) => false);
    } else {
      return into(dbJobs).insert(job.job).then((jobId) {
        if (jobId >= 0) {
          job.job = job.job.copyWith(id: Value(jobId));
          _insertJobSteps(job);
          return true;
        } else {
          return false;
        }
      }).catchError((_) => false);
    }
  }

  Future<bool> remove(final BreadlyJob job) {
    if (!job.job.id.present) return Future.value(true);

    _deleteJobSteps(job);
    return (delete(dbJobs)..where((j) => j.id.equals(job.job.id.value)))
        .go()
        .then((value) => value > 0)
        .catchError((_) => false);
  }
}
