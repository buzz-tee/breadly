import 'package:breadly/model/model.dart';
import 'package:moor/moor.dart';

part 'profiles_dao.g.dart';

@UseDao(tables: [DbJobs, DbProfiles, DbProfileSteps])
class ProfilesDao extends DatabaseAccessor<BreadlyDatabase> with _$ProfilesDaoMixin {
  ProfilesDao(final BreadlyDatabase db) : super(db);

  Future<List<BreadlyProfile>> getAll() {
    return (
        select(dbProfiles)
            .join([
          leftOuterJoin(
              dbProfileSteps, dbProfileSteps.profileId.equalsExp(dbProfiles.id))
        ])
          ..orderBy([OrderingTerm.asc(dbProfileSteps.index)])
    )
        .get()
        .then((rows) {
      final groupedData = <DbProfile, List<DbProfileStep>>{};
      for (final row in rows) {
        final profile = row.readTable(dbProfiles);
        final profileStep = row.readTableOrNull(dbProfileSteps);
        final profileStepList = groupedData.putIfAbsent(profile, () => []);
        if (profileStep != null) profileStepList.add(profileStep);
      }
      return [
        for (final entry in groupedData.entries.toList(growable: false)
          ..sort((a, b) =>
              a.key.name.toLowerCase().compareTo(b.key.name.toLowerCase())))
          BreadlyProfile.from(entry.key, entry.value)
      ];
    });
  }

  void _deleteProfileSteps(final BreadlyProfile profile) {
    if (profile.profile.id.present) {
      (delete(dbProfileSteps)
        ..where((s) => s.profileId.equals(profile.profile.id.value))).go();
    }
  }

  void _insertProfileSteps(final BreadlyProfile profile) {
    profile.steps.forEach((step) {
      step = step.copyWith(profileId: profile.profile.id);
      into(dbProfileSteps).insert(step);
    });
  }

  Future<bool> save(final BreadlyProfile profile) {
    if (profile.profile.id.present) {
      _deleteProfileSteps(profile);
      _insertProfileSteps(profile);

      return (update(dbProfiles)
        ..where((p) => p.id.equals(profile.profile.id.value)))
          .write(profile.profile)
          .then((value) => value > 0)
          .catchError((_) => false);
    } else {
      // New profile
      return into(dbProfiles).insert(profile.profile).then((profileId) {
        if (profileId >= 0) {
          profile.profile = profile.profile.copyWith(id: Value(profileId));
          _insertProfileSteps(profile);
          return true;
        } else {
          return false;
        }
      }).catchError((_) => false);
    }
  }

  Future<bool> remove(final BreadlyProfile profile) {
    if (!profile.profile.id.present)
      return Future.value(true);

    _deleteProfileSteps(profile);
    return (delete(dbProfiles)
      ..where((p) => p.id.equals(profile.profile.id.value)))
        .go()
        .then((value) => value > 0)
        .catchError((_) => false);
  }

  Future<List<DbJob>> usedBy(final BreadlyProfile profile) {
    if (!profile.profile.id.present)
      return Future.value([]);

    return (select(dbJobs)
      ..where((job) => job.profileId.equals(profile.profile.id.value)))
        .get();
  }
}