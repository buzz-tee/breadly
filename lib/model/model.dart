import 'dart:io';

import 'package:breadly/model/profiles_dao.dart';
import 'package:moor/ffi.dart';
import 'package:moor/moor.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as path;

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'jobs_dao.dart';

part 'model.g.dart';

enum AlarmConfig {
  AlarmOn,
  AlarmOnAutoAdvance,
  AlarmOff,
}

class DbJobs extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get name => text()();
  IntColumn get profileId => integer()();

  IntColumn get currentStepIndex => integer().withDefault(const Constant(-1))();
  DateTimeColumn get currentStepStart =>
      dateTime().withDefault(currentDateAndTime)();

  @override
  get customConstraints => [
        'FOREIGN KEY (profile_id) references profile (id) ON DELETE RESTRICT',
      ];

  @override
  get tableName => 'jobs';
}

class DbJobSteps extends Table {
  IntColumn get jobId => integer()();
  IntColumn get index => integer()();
  TextColumn get name => text()();
  IntColumn get duration => integer()();
  IntColumn get maxDuration => integer().nullable()();
  IntColumn get alarmConfig => intEnum<AlarmConfig>()();

  @override
  get customConstraints => [
        'PRIMARY KEY (job_id, "index")',
        'FOREIGN KEY (job_id) REFERENCES jobs (id) ON DELETE CASCADE',
      ];

  @override
  get tableName => 'job_steps';
}

class DbProfiles extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get name => text()();
  TextColumn get description => text().nullable()();

  @override
  get customConstraints => [
        'UNIQUE (name)',
      ];

  @override
  get tableName => 'profiles';
}

class DbProfileSteps extends Table {
  IntColumn get profileId => integer()();
  IntColumn get index => integer()();
  TextColumn get name => text()();
  IntColumn get duration => integer()();
  IntColumn get maxDuration => integer().nullable()();
  IntColumn get alarmConfig => intEnum<AlarmConfig>()();

  @override
  get customConstraints => [
        'PRIMARY KEY (profile_id, "index")',
        'FOREIGN KEY (profile_id) REFERENCES profiles (id) ON DELETE CASCADE',
      ];

  @override
  get tableName => 'profile_steps';
}

@UseMoor(
    tables: [DbJobs, DbJobSteps, DbProfiles, DbProfileSteps],
    daos: [JobsDao, ProfilesDao])
class BreadlyDatabase extends _$BreadlyDatabase {
  static LazyDatabase _openConnection() {
    return LazyDatabase(() async {
      final dbFolder = await getApplicationDocumentsDirectory();
      final file = File(path.join(dbFolder.path, 'breadly.moor.db'));
      return VmDatabase(file);
    });
  }

  BreadlyDatabase() : super(_openConnection());

  @override
  int get schemaVersion => 1;
}

class FlexibleDuration {
  int duration;
  int? maxDuration;

  FlexibleDuration({this.duration = 0, this.maxDuration});

  String toLocalizedString(final AppLocalizations locale) {
    bool durationUnits = (maxDuration != null);

    String hourSingular = durationUnits
        ? locale.generic__hour_unit
        : locale.generic__hour_singular;
    String hourPlural =
        durationUnits ? locale.generic__hour_unit : locale.generic__hour_plural;
    String minuteSingular = durationUnits
        ? locale.generic__minute_unit
        : locale.generic__minute_singular;
    String minutePlural = durationUnits
        ? locale.generic__minute_unit
        : locale.generic__minute_plural;

    final StringBuffer result = StringBuffer();
    final writeDuration = (final int duration) {
      int hours = (duration / 60).truncate();
      int minutes = duration % 60;
      if (hours > 0)
        result.write('$hours ${hours == 1 ? hourSingular : hourPlural}');
      if (hours > 0 && minutes > 0) result.write(' ');
      if (hours == 0 || minutes > 0)
        result
            .write('$minutes ${minutes == 1 ? minuteSingular : minutePlural}');
    };

    writeDuration(duration);
    if (maxDuration != null) {
      result.write(' - ');
      writeDuration(maxDuration!);
    }
    return result.toString();
  }
}

class BreadlyProfile {
  DbProfilesCompanion profile;
  final List<DbProfileStepsCompanion> steps;

  BreadlyProfile()
      : profile = DbProfilesCompanion(),
        steps = [];

  BreadlyProfile.from(DbProfile profile, List<DbProfileStep> steps)
      : profile = profile.toCompanion(true),
        steps = List.generate(
            steps.length, (index) => steps[index].toCompanion(true));

  String? get name => profile.name.present ? profile.name.value : null;

  set name(value) => profile = profile.copyWith(name: Value(value));

  String? get description => profile.description.value;

  set description(value) =>
      profile = profile.copyWith(description: Value(value));

  DbProfileStepsCompanion addStep(
      {required final String name,
      required final FlexibleDuration duration,
      required final AlarmConfig alarmConfig}) {
    final DbProfileStepsCompanion step = DbProfileStepsCompanion(
      index: Value(steps.length),
      name: Value(name),
      duration: Value(duration.duration),
      maxDuration: Value(duration.maxDuration),
      alarmConfig: Value(alarmConfig),
    );
    steps.add(step);
    return step;
  }

  FlexibleDuration get totalDuration {
    int duration = 0;
    int maxDuration = 0;
    for (final step in steps) {
      duration += step.duration.value;
      maxDuration += (step.maxDuration.value != null &&
              step.maxDuration.value! > step.duration.value)
          ? step.maxDuration.value!
          : step.duration.value;
    }
    return FlexibleDuration(
      duration: duration,
      maxDuration: (maxDuration > duration) ? maxDuration : null,
    );
  }
}

class BreadlyJobStep {
  DbJobStepsCompanion step;

  String? get name => step.name.present ? step.name.value : null;
  int? get index => step.index.present ? step.index.value : null;
  set index(final int? value) => step =
      step.copyWith(index: value == null ? Value.absent() : Value(value));

  int get jobId => step.jobId.value;
  set jobId(final int value) => step = step.copyWith(jobId: Value(value));

  AlarmConfig get alarmConfig => step.alarmConfig.value;

  Duration get duration => Duration(minutes: step.duration.value);
  Duration get maxDuration => Duration(
      minutes: step.maxDuration.present
          ? step.maxDuration.value!
          : step.duration.value);

  DateTime getEndTime(final DateTime startTime) => startTime.add(duration);
  DateTime getLatestEndTime(final DateTime startTime) =>
      startTime.add(maxDuration);

  BreadlyJobStep({required this.step});
}

class BreadlyJob {
  DbJobsCompanion job;
  DbProfilesCompanion profile;
  List<BreadlyJobStep> steps;

  BreadlyJob()
      : job = DbJobsCompanion(),
        profile = DbProfilesCompanion(),
        steps = [];

  BreadlyJob.from(final DbJob job, final DbProfile? profile, this.steps)
      : job = job.toCompanion(true),
        profile = profile?.toCompanion(true) ?? DbProfilesCompanion();

  void apply({
    required final String name,
    required final int currentStepIndex,
    required final DateTime currentStepStart,
  }) {
    job = job.copyWith(
      name: Value(name),
      currentStepIndex: Value(currentStepIndex),
      currentStepStart: Value(currentStepStart),
    );
  }

  String? get name => job.name.present ? job.name.value : null;
  DateTime get currentStepStart => job.currentStepStart.value;

  BreadlyJobStep? startNextStep() {
    final step = getCurrentStep();
    if (step != null || job.currentStepIndex.value == -1) {
      job = job.copyWith(
        currentStepStart: Value(DateTime.now()),
        currentStepIndex: Value(job.currentStepIndex.value + 1),
      );
      return getCurrentStep();
    }
    return null;
  }

  BreadlyJobStep? getNextStep() {
    final _ = getCurrentStep();
    if (job.currentStepIndex.value + 1 < steps.length) {
      return steps[job.currentStepIndex.value + 1];
    }
    return null;
  }

  BreadlyJobStep? getCurrentStep() {
    // currentStepIndex == -1: not started yet
    // currentStepIndex == steps.length: all steps finished

    var currentStepIndex = job.currentStepIndex.value;
    var currentStepStart = job.currentStepStart.value;

    final now = DateTime.now();

    if (currentStepIndex < 0 && currentStepStart.isBefore(now))
      currentStepIndex = 0;

    while (currentStepIndex >= 0 && currentStepIndex < steps.length) {
      final step = steps[currentStepIndex];
      final nextStepStart = step.getLatestEndTime(currentStepStart);

      if (currentStepStart.isBefore(now) && nextStepStart.isAfter(now)) {
        job = job.copyWith(
            currentStepStart: Value(currentStepStart),
            currentStepIndex: Value(currentStepIndex));
        return step;
      }

      currentStepIndex += 1;
      currentStepStart = nextStepStart;
    }

    job = job.copyWith(
        currentStepStart: Value(currentStepStart),
        currentStepIndex: Value(currentStepIndex));
    return null;
  }

  static BreadlyJobStep jobStepFromProfileStep(
      final DbProfileStepsCompanion profileStep) {
    return BreadlyJobStep(
        step: DbJobStepsCompanion(
      index: profileStep.index,
      name: profileStep.name,
      duration: profileStep.duration,
      maxDuration: profileStep.maxDuration,
      alarmConfig: profileStep.alarmConfig,
    ));
  }

  void addSteps(final List<DbProfileStepsCompanion> profileSteps) {
    steps.clear();
    for (final profileStep in profileSteps) {
      var jobStep = jobStepFromProfileStep(profileStep);
      if (jobStep.index == null) jobStep.index = steps.length;
      steps.add(jobStep);
    }
  }
}
